package com.xfdmao.fcat.coin.service.impl;

import com.xfdmao.fcat.coin.entity.BtcHour;
import com.xfdmao.fcat.coin.mapper.BtcHourMapper;
import com.xfdmao.fcat.coin.service.BtcHourService;
import com.xfdmao.fcat.common.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by fier on 2018/09/20
 */
@Service
public class BtcHourServiceImpl extends BaseServiceImpl<BtcHourMapper,BtcHour> implements BtcHourService{
    @Override
    public Boolean batchInsert(List<BtcHour> btcHourList) {
        return mapper.batchInsert(btcHourList);
    }

    @Override
    public List<BtcHour> getByPeriod(String period) {
        BtcHour btcHour = new BtcHour();
        btcHour.setPeriod(period);
        return mapper.select(btcHour);
    }
}
